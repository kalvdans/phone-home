Phone Home SSH Service
======================

This script provides a way to make devices connected to the internet reachable for each other.

### Prerequisites

A publicly reachable SSH server with a user designated for tunneling.

This user should be configured in the sshd_config file to not allow any other action, using the ForceCommand directive:

```
Match User restricted
   ForceCommand (cat /etc/ssh/tunnel_hosts)

```

The file /etc/ssh/tunnel_hosts should contain a list of available hosts and their designated ports:

```
# Host      Address      User      SSH-port  VNC-port
office      localhost    mrSmith   11001     11101
laptop      localhost    peter     11002     11102
kitchen     localhost    foo       11003     11103
parents     localhost    dad       11004     11104
htpc        192.168.0.5  xbmc      22        5900
```
Blank lines and lines starting with `#` are ignored.

### Installation

Run generate.sh to install the service. You will be asked to specify the remote host to connect to, which other remote clients you want to create local tunnels to, and where to store the ssh key. You will also be asked to verify any install paths used by the script.

The service can be installed either as a system service or as a user service, depending on which user the generate.sh script is run as.

The service will automatically patch the specified ssh config file to set up aliases for all desired ssh tunnels:
```
Host kitchen
  Hostname localhost
  User foo
  Port 2222
  
Host laptop
  Hostname localhost
  User peter
  Port 2223
```
This enabled you to easily connect to online remote clients thusly:
```
[foo@bar ~]$ ssh laptop
peter@localhost's password:
```
