#!/bin/bash

servicepath="$HOME/.config/systemd/user"
scriptpath="$HOME/bin"
keypath="$HOME/.ssh"
SSH_CONFIG="$HOME/.ssh/config"

if (( EUID == 0 ))
then
  servicepath="/etc/systemd/system"
  scriptpath="/usr/local/sbin"
  keypath="/etc/ssh"
  SSH_CONFIG="/etc/ssh/ssh_config"
fi

while [[ -z "$ID" ]]
do
  read -p "Enter address to remote host (user@host): " HOST
  ID=$HOST
  ID=${ID//@/-}
  ID=${ID//\./_}
  ID=${ID//[^a-zA-Z0-9_\-]/}
done

SERVICE="$servicepath/phone-home_$ID.service"
SCRIPT="$scriptpath/phone-home"
KEY="$keypath/phone-home-key_$ID"

read -e -p "Enter systemd service install path [ $SERVICE ]: " input
SERVICE="${input:-$SERVICE}"

read -e -p "Enter phone-home script install path [ $SCRIPT ]: " input
SCRIPT="${input:-$SCRIPT}"

read -e -p "Enter path to key used for host '$HOST' [ $KEY ]: " input
KEY="${input:-$KEY}"

read -e -p "Enter path to ssh config [ $SSH_CONFIG ]: " input
SSH_CONFIG=${input:-$SSH_CONFIG}


if ! [ -e "$KEY" ]
then
  echo "Generating SSH key:"
  ssh-keygen -N "" -f "$KEY"
fi

if ! ssh -q -i "$KEY" -o PasswordAuthentication=no $HOST "echo 2>&1"
then
  echo <<EOF
Could not connect to host.
Please add the following public SSH key to .ssh/authorized_keys on '$HOST' manually:

EOF
  cat "$KEY.pub"
  echo
fi

echo <<EOF
Please enter the hosts you want to set up a tunnels to,
on the form \"host1:localport1\". Separate hosts with blankspace.
EOF

ENTRY="[[:alnum:]]+:[[:digit:]]+"
REGEX="^($ENTRY([[:blank:]]+$ENTRY)*)?$"

SSH="INVALID"
while ! [[ "$SSH" =~ $REGEX ]]
do
  read -e -p "SSH tunnels []: " SSH
done

VNC="INVALID"
while ! [[ "$VNC" =~ $REGEX ]]
do
  read -e -p "VNC tunnels []: " VNC
done

echo -n "Creating service file '$SERVICE'..."
mkdir -p "$(dirname $SERVICE)"
cat > "$SERVICE" <<EOF
[Unit]
Description=Phone home SSH service for host $HOST
After=network.target

[Service]
ExecStart=$SCRIPT "$HOST" "$KEY" "$SSH" "$VNC" "$SSH_CONFIG"

RestartSec=120
Restart=always

[Install]
WantedBy=default.target
EOF
echo "done."

echo -n "Copying phone-home.sh to '$SCRIPT'..."
mkdir -p "$(dirname $SCRIPT)"
cp "$(dirname $0)/phone-home.sh" "$SCRIPT"
echo "done."

servicefile=$(basename $SERVICE)
echo -n "Enabling systemd service..."
if (( EUID == 0 ))
then
  systemctl daemon-reload
  systemctl enable $servicefile
  systemctl start $servicefile
else
  systemctl --user daemon-reload
  systemctl --user enable $servicefile
  systemctl --user start $servicefile
fi
echo "done."
