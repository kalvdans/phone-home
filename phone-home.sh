#!/bin/bash

FLAGS="-o ServerAliveInterval=15 -o ServerAliveCountMax=3 -o ConnectTimeout=15 -o TCPKeepAlive=yes -o ExitOnForwardFailure=yes -o StrictHostKeyChecking=yes -o BatchMode=yes"

if [ $# -lt 4 ]
then
  echo "Usage: $(basename $0) user@remotehost sshkey 'host:sshport ...' 'host:vncport ...' sshconfig"
  exit 0;
fi

HOST="$1"
KEY="$2"
SSH="$3"
VNC="$4"
SSH_CONFIG_FILE="$5"

ENTRY="[[:alnum:]]+:[[:digit:]]+"
REGEX="^($ENTRY([[:blank:]]+$ENTRY)*)?$"

if ! [[ "$SSH" =~ $REGEX ]]
then
  echo "Invalid ssh argument. Excpected 'host1:port1 host2:port2 ...'"
  exit 1
elsif ! [[ "$VNC" =~ $REGEX ]]
  echo "Invalid vnc argument. Excpected 'host1:port1 host2:port2 ...'"
  exit 1
elsif ! [ -r "$KEY" ]
  echo "SSH key file not readable"
  exit 1
elsif ! [ -w "$SSH_CONFIG_FILE" ]
  echo "SSH config file not writeable"
  exit 1
elsif ! [ -z "$HOST" ]
  echo "Remote host must be set"
  exit 1
fi


LOCAL=""
REMOTE=""
SSH_CONFIG=""


FLAGS="-i \"$KEY\" $FLAGS"
hosts=`ssh $FLAGS $HOST`
hostname=`hostname`

while read line
do
  [ -z "$line" ] && continue
  [[ "$line" == \#* ]] && continue

  read host address user ssh vnc <<< "$line"

  if [[ $host == $hostname ]]
  then
    echo "Setting up reverse SSH and VNC tunnels for $host"
    REMOTE="$REMOTE -R $ssh:localhost:22"
    REMOTE="$REMOTE -R $vnc:localhost:5900"
  fi

  for tunnel in $SSH
  do
    thost=${tunnel%:*}
    tport=${tunnel#*:}

    if [[ $host == $thost ]]
    then
      echo "Setting up local SSH tunnel for $host"
      LOCAL="$LOCAL -L $tport:$address:$ssh"

      SSH_CONFIG="$SSH_CONFIG
Host $host
  Hostname localhost
  User $user
  Port $tport"

    fi
  done

  for tunnel in $VNC
  do
    thost=${tunnel%:*}
    tport=${tunnel#*:}

    if [[ $host == $thost ]]
    then
      echo "Setting up local VNC tunnel for $host"
      LOCAL="$LOCAL -L $tport:$address:$vnc"
    fi
  done

done <<< "$hosts"

bstr="#BEGIN AUTO GENERATED PHONE_HOME CONFIG"
astr="#END AUTO GENERATED PHONE_HOME CONFIG"

str="$bstr
$SSH_CONFIG
$astr"

if grep -q "$bstr" $SSH_CONFIG_FILE
then
  TMP=`mktemp`
  echo "$str" >> $TMP
  sed -i -e "/^$astr/r $TMP" -e "/^$bstr/,/^$astr/d" $SSH_CONFIG_FILE
  rm $TMP
else
  echo "$str" >> $SSH_CONFIG_FILE
fi

ssh -NTC $FLAGS $REMOTE $LOCAL $HOST
